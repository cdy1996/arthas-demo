package com.cdy.demo.agent;

public class CommandLineContext {

    /**
     * @see CommandTypeConstant
     */
    private volatile String commandType;

    private volatile String commandClass;

    private volatile String commandMethod;

    private volatile Integer times;

    private volatile Thread thread;

    public String getCommandType() {
        return commandType;
    }

    public void setCommandType(String commandType) {
        this.commandType = commandType;
    }

    public String getCommandClass() {
        return commandClass;
    }

    public void setCommandClass(String commandClass) {
        this.commandClass = commandClass;
    }

    public String getCommandMethod() {
        return commandMethod;
    }

    public void setCommandMethod(String commandMethod) {
        this.commandMethod = commandMethod;
    }

    public Integer getTimes() {
        return times;
    }

    public void setTimes(Integer times) {
        this.times = times;
    }

    public Thread getThread() {
        return thread;
    }

    public void setThread(Thread thread) {
        this.thread = thread;
    }
}
