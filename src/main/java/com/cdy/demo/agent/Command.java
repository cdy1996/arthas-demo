package com.cdy.demo.agent;


public interface Command {
    boolean meet();

    byte[] transform(CommandContext commandContext);
}
