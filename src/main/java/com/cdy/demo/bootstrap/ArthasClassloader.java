package com.cdy.demo.bootstrap;

import java.net.URL;
import java.net.URLClassLoader;
import java.net.URLStreamHandlerFactory;

public class ArthasClassloader extends URLClassLoader {
    public ArthasClassloader(URL[] urls, ClassLoader parent) {
        super(urls, parent);
    }

    public ArthasClassloader(URL[] urls) {
        super(urls);
    }

    public ArthasClassloader(URL[] urls, ClassLoader parent, URLStreamHandlerFactory factory) {
        super(urls, parent, factory);
    }
}
