package com.cdy.demo.attach;

import com.sun.tools.attach.*;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;


public class JVMTIThread {

    public static void main(String[] args) throws IOException, AttachNotSupportedException, AgentLoadException, AgentInitializationException, ClassNotFoundException, InstantiationException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        System.out.println(Thread.currentThread().getContextClassLoader());

        List<VirtualMachineDescriptor> list = VirtualMachine.list();
        for (VirtualMachineDescriptor vmd : list) {
            if (vmd.displayName().endsWith("Main")) {
                VirtualMachine virtualMachine = VirtualMachine.attach(vmd.id());
                virtualMachine.loadAgent("/Users/chendongyi/Downloads/ideaProjects/demo_maven/target/demo_maven-1.0-SNAPSHOT.jar", "cxs");
                System.out.println("ok");
                virtualMachine.detach();
                break;
            }
        }
        System.out.println("已加载agent");
        System.in.read();
    }

}