package com.cdy.demo;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;

/**
 * 模拟应用
 * https://shimo.im/mindmaps/rJRhThTRRqrxt8vW
 */
public class Main {


    public static void main(String[] args) throws InterruptedException, ClassNotFoundException, InstantiationException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        URLClassLoader urlClassLoader = new URLClassLoader(new URL[]{}, Thread.currentThread().getContextClassLoader());
        Thread.currentThread().setContextClassLoader(urlClassLoader);
        Class<?> aClass = urlClassLoader.loadClass("com.cdy.demo.Controller");
        Object o = aClass.newInstance();
        Method main1 = aClass.getMethod("hello");
        for (; ; ) {
            main1.invoke(o);
            Thread.sleep(1000L);
        }

    }


}
